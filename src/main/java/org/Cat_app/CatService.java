package org.Cat_app;


import com.google.gson.Gson;
import okhttp3.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


public class CatService {

        public static void ViewCat() throws IOException {

            // Se traen las imagenes de los gaticos
            OkHttpClient client = new OkHttpClient().newBuilder().build();

            Request request = new Request.Builder().url("https://api.thecatapi.com/v1/images/search").get().build();
            Response response = client.newCall(request).execute();

            String theJson = response.body().string();

            //Cortar los corchetes
            theJson = theJson.substring(1, theJson.length());
            theJson = theJson.substring(0, theJson.length() - 1);



            //GSON es un API en Java, desarrollada por Google, que se utiliza para convertir
            // objetos Java a JSON (serialización) y JSON a objetos Java (deserialización)

            // Crear objeto de la clase gson
            Gson gson = new Gson();
            Cats cats = gson.fromJson(theJson, Cats.class);

            //Redimencionar Imagenes
            Image image = null;
            try {
                URL url = new URL(cats.getUrl());
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.addRequestProperty("User-Agent", "");
                BufferedImage bufferedImage = ImageIO.read(http.getInputStream());
                ImageIcon BackgroudCat = new ImageIcon(bufferedImage);

                if (BackgroudCat.getIconWidth() > 800 || BackgroudCat.getIconHeight() > 400) {

                    Image fondo = BackgroudCat.getImage();
                    Image modificada = fondo.getScaledInstance(500, 500, java.awt.Image.SCALE_SMOOTH);
                    BackgroudCat = new ImageIcon(modificada);
                }



//                String menu = "Options:\n "
//                        + "1. Ver otra imagen \n"
//                        + "2. Favorito \n"
//                        + "3. Volver \n";

                String[] buttons = {"Ver otra imagen", "Favorito","Volver"};
                String id_cat = cats.getId();
                String Option = (String) JOptionPane.showInputDialog(null,null,id_cat,JOptionPane.INFORMATION_MESSAGE,BackgroudCat,buttons, buttons[0]);

                int  select = -1;

                for(int i=0;i<buttons.length;i++){
                    if(Option.equals(buttons[i])){
                        select = i;
                    }
                }

                switch (select){
                    case 0:
                        ViewCat();
                        break;
                    case 1:
                        favoriteCat(cats);
                        break;
                    default:
                        break;

                }

            }catch (IOException e){
                System.out.println(e);

            }

        }
        public static void favoriteCat(Cats cats){

            try {
                OkHttpClient client = new OkHttpClient().newBuilder().build();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, "{\n\t\"image_id\":\""+cats.getId()+"\"\n}");
                Request request = new Request.Builder()
                        .url("https://api.thecatapi.com/v1/favourites?")
                        .method("POST", body)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("x-api-key", cats.getApikey()).build();
                Response response = client.newCall(request).execute();

            }catch (IOException e){
                System.out.println(e);
            }

        }

    public static void ViewFavorite(String apikey) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Request request = new Request.Builder()

                .url("https://api.thecatapi.com/v1/favourites").get()
                .addHeader("x-api-key", apikey).build();

        Response response = client.newCall(request).execute();

        String theJson = response.body().string();

        Gson gson = new Gson();

        CatFavorite [] CatsArray = gson.fromJson(theJson,CatFavorite[].class);

        if(CatsArray.length > 0){
            int min = 1;
            int max  = CatsArray.length;
            int random = (int) (Math.random() * ((max-min)-1)) + min;
            int indice = random-1;

            CatFavorite catFav = CatsArray[indice];

            Image image = null;
            try{
                URL url = new URL(catFav.Image.getUrl());
                image = ImageIO.read(url);

                ImageIcon fondoGato = new ImageIcon(image);

                if(fondoGato.getIconWidth() > 800){

                    //redimensionamos
                    Image fondo = fondoGato.getImage();
                    Image modificada = fondo.getScaledInstance(800, 600, java.awt.Image.SCALE_SMOOTH);
                    fondoGato = new ImageIcon(modificada);
                }
//
//                String menu = "Opciones: \n"
//                        + " 1. ver otra imagen \n"
//                        + " 2. Eliminar Favorito \n"
//                        + " 3. Volver \n";

                String[] botones = { "ver otra imagen", " Eliminar favorito", "eliminar favorito", "volver" };
                String id_gato = catFav.getId();
                String opcion = (String) JOptionPane.showInputDialog(null,null,id_gato, JOptionPane.INFORMATION_MESSAGE, fondoGato, botones,botones[0]);

                int seleccion = -1;
                //validamos que opcion selecciona el usuario
                for(int i=0;i<botones.length;i++){
                    if(opcion.equals(botones[i])){
                        seleccion = i;
                    }
                }

                switch (seleccion){
                    case 0:
                        ViewFavorite(apikey);
                        break;
                    case 1:
                        deteleteFavorite(catFav);
                        break;
                    default:
                        break;
                }

            }catch(IOException e){
                System.out.println(e);
            }

        }

    }

    public static void deteleteFavorite(CatFavorite catFavorite){

    }



}






