package org.Cat_app;
import javax.swing.*;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
            int Options_menu = -1;
            String[] buttons = {" 1. ver gatos", "2. Ver favoritos","3. salir"};

            do{

                //menu principal
                String options
                        = (String) JOptionPane.showInputDialog(null, "My Cat Java", "Menu principal", JOptionPane.INFORMATION_MESSAGE,
                        null, buttons,buttons[0]);

                //validamos que opcion selecciona el usuario

                for(int i=0;i<buttons.length;i++){
                    if(options.equals(buttons[i])){
                        Options_menu = i;
                    }
                }

                switch(Options_menu){
                    case 0:
                        CatService.ViewCat();
                        break;
                    case 1:
                        Cats cat = new Cats();
                       CatService.ViewFavorite(cat.getApikey());
                       break;
                    default:
                        break;
                }
            }while(Options_menu != 1);


    }

}