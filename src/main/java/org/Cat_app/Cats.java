package org.Cat_app;

public class Cats {
    String id;
    String url;
    String apikey = "b515db6c-66d6-4c70-b56f-3da8a0b17650";
    ImagenFav image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public ImagenFav getImage() {
        return image;
    }

    public void setImage(ImagenFav image) {
        this.image = image;
    }
}
