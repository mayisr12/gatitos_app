package org.Cat_app;

public class CatFavorite {
        String id;
        String image_id;
        String apikey = "b515db6c-66d6-4c70-b56f-3da8a0b17650";
        ImagenFav Image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage_id() {
            return image_id;
        }

        public void setImage_id(String image_id) {
            this.image_id = image_id;
        }

        public String getApikey() {
            return apikey;
        }

        public void setApikey(String apikey) {
            this.apikey = apikey;
        }

        public ImagenFav getImage() {
            return Image;
        }

        public void setImage(ImagenFav image) {
            Image = image;
        }

}

